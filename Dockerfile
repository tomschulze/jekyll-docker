# https://github.com/BretFisher/jekyll-serve
FROM ruby:3.3.0-alpine3.18 as jekyll

RUN apk --update --virtual .build-deps add g++ gcc make libffi-dev
RUN gem update --system && \
  gem install jekyll && \
  gem install bundler && \
  gem cleanup
RUN apk del .build-deps

# for jekyll new-theme command
RUN apk --update add git
RUN git config --global init.defaultBranch main

COPY docker-entrypoint.sh /usr/local/bin

WORKDIR /site

EXPOSE 4000

ENTRYPOINT [ "jekyll" ]
CMD [ "help" ]

FROM jekyll as jekyll-serve

ENTRYPOINT [ "docker-entrypoint.sh" ]
CMD [ "bundle", "exec", "jekyll", "serve", "-H", "0.0.0.0", "-P", "4000" ]
