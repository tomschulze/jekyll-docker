## Jekyll-docker

Docker image heavily based on [bretfisher/jekyll-serve](https://github.com/BretFisher/jekyll-serve).

The differences are:
* an alpine base image is used
* git is installed for the `jekyll new-theme` command
* gcc, g++ and make are deleted after jekyll is installed to reduce image size

Run `./build-locally.sh` to create the images locally. Then use as described in the 
[original docs](https://github.com/BretFisher/jekyll-serve).

* `docker run --rm  -v $(pwd):/site jekyll new .`
* `docker run --rm  -v $(pwd):/site jekyll-serve`



